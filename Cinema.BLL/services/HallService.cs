﻿using Cinema.DAL.Domain;
using Cinema.DAL.Domain.Exceptions;
using Cinema.DAL.Helpers;
using Cinema.DAL.Repositories;
using Cinema.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema.BLL.services
{
    public interface IHallService
    {
        Task<HallDTO> Get(string hallNumber);
        Task<IEnumerable<HallDTO>> GetAllAsync();
        Task AddHallAsync(string hallNumber, int seatsInRow, int numberOfRows);
        Task UpdateAsync(Hall hall);
        Task Remove(string hallNumber);
    }
    public class HallService : IHallService
    {
        private readonly IHallRepository _hallRepository;
        public HallService(IHallRepository hallRepository)
        {
            _hallRepository = hallRepository;
        }
        public async Task<HallDTO> Get(string hallNumber)
        {
            var hall = await _hallRepository.GetAsync(hallNumber);
            return hall.MapHall();
        }
        public async Task<IEnumerable<HallDTO>> GetAllAsync()
        {
            var halls = await _hallRepository.GetAllAsync();
            return halls.Select(x => x.MapHall());
        }
        public async Task AddHallAsync(string hallNumber, int seatsInRow, int numberOfRows)
        {
            if(await _hallRepository.DoesExist(hallNumber))
            {
                throw new CinemaException(ErrorCodes.AlreadyExist, $"Hall with number {hallNumber} already exists.");
            }
            var hall = new Hall(hallNumber.Trim(), seatsInRow, numberOfRows);
            await _hallRepository.AddAsync(hall);
        }
    public async Task Remove(string hallNumber)
        {
            await _hallRepository.RemoveAsync(hallNumber);
        }
        public async Task UpdateAsync(Hall hall)
        {
            await _hallRepository.UpdateAsync(hall);
        }
    }
}
