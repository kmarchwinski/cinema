﻿using Cinema.BLL.services;
using Cinema.DAL;
using Cinema.DAL.Domain;
using Cinema.DAL.Domain.Exceptions;
using Cinema.DAL.Repositories;
using Cinema.Shared.DTO;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CinemaTests
{
    public class OrderServiceTests
    {
        [Fact]
        public async Task Order_service_should_throw_exception_when_pefromance_does_not_exist()
        {
            var performanceRepositoryMock = new Mock<IPerformanceRepository>();
            var orderRepositoryMock = new Mock<IOrderRepository>();
            var hallRepositoryMock = new Mock<IHallRepository>();
            performanceRepositoryMock.Setup(x => x.GetAsync(It.IsAny<Guid>())).ReturnsAsync((Performance)null);

            var service = new OrderService(orderRepositoryMock.Object, hallRepositoryMock.Object, performanceRepositoryMock.Object);
            var bookedSeats = new List<BookedSeatDTO>();
            var exception = await Assert.ThrowsAsync<CinemaException>(() => service.SubmitOrder(Guid.NewGuid(), bookedSeats));
            Assert.Equal(ErrorCodes.NotExist, exception.Code);
        }
        [Fact]
        public async Task Order_service_should_throw_exception_when_seat_is_already_reserved()
        {
            var performanceRepositoryMock = new Mock<IPerformanceRepository>();
            var orderRepositoryMock = new Mock<IOrderRepository>();
            var hallRepositoryMock = new Mock<IHallRepository>();

            performanceRepositoryMock.Setup(x => x.GetAsync(It.IsAny<Guid>())).ReturnsAsync(new Performance());
            orderRepositoryMock.Setup(x => x.IsSeatReserved(It.IsAny<Guid>(), It.IsAny<int>(), It.IsAny<int>())).ReturnsAsync(true);
            hallRepositoryMock.Setup(x => x.GetAsync(It.IsAny<string>())).ReturnsAsync(new Hall());

            var service = new OrderService(orderRepositoryMock.Object, hallRepositoryMock.Object, performanceRepositoryMock.Object);
            var bookedSeats = new List<BookedSeatDTO>();
            bookedSeats.Add(new BookedSeatDTO());

            var exception = await Assert.ThrowsAsync<CinemaException>(() => service.SubmitOrder(Guid.NewGuid(), bookedSeats));
            Assert.Equal(ErrorCodes.AlreadyReserved, exception.Code);
        }

        [Fact]
        public async Task Order_service_should_throw_exception_when_seat_is_not_available()
        {
            var performanceRepositoryMock = new Mock<IPerformanceRepository>();
            var orderRepositoryMock = new Mock<IOrderRepository>();
            var hallRepositoryMock = new Mock<IHallRepository>();
            var service = new OrderService(orderRepositoryMock.Object, hallRepositoryMock.Object, performanceRepositoryMock.Object);

            performanceRepositoryMock.Setup(x => x.GetAsync(It.IsAny<Guid>())).ReturnsAsync(new Performance());

            var exampleHall = new Hall("1", 2, 2);
            var exampleBooking = new BookedSeatDTO()
            {
                RowNumber = 5,
                SeatNumber = 5
            };
            var bookedSeats = new List<BookedSeatDTO>();
            bookedSeats.Add(exampleBooking);

            hallRepositoryMock.Setup(x => x.GetAsync(It.IsAny<string>())).ReturnsAsync(exampleHall);
            var exception = await Assert.ThrowsAsync<CinemaException>(() => service.SubmitOrder(Guid.NewGuid(), bookedSeats));
            Assert.Equal(ErrorCodes.InvalidSeatData, exception.Code);
        }

        [Fact]
        public async Task Order_service_should_get_expected_data_when_invoke_GetbyID_method()
        {
            var expectedOrderId = Guid.NewGuid();
            var expectedSeat = 10;
            var expectedRow = 4;
            var expectedPerfomranceId = Guid.NewGuid();
            var performanceRepositoryMock = new Mock<IPerformanceRepository>();
            var orderRepositoryMock = new Mock<IOrderRepository>();
            var hallRepositoryMock = new Mock<IHallRepository>();
            orderRepositoryMock.Setup(x => x.GetAsync(It.IsAny<Guid>())).ReturnsAsync(new Order(expectedOrderId, expectedPerfomranceId, expectedSeat, expectedRow));

            var service = new OrderService(orderRepositoryMock.Object, hallRepositoryMock.Object, performanceRepositoryMock.Object);
            var result = await service.GetbyId(Guid.NewGuid());

            Assert.Equal(expectedRow, result.Row);
            Assert.Equal(expectedSeat, result.SeatNumber);
            Assert.Equal(expectedPerfomranceId, result.MyPerformanceId);
            Assert.Equal(expectedOrderId, result.OrderId);
        }
    }
}
