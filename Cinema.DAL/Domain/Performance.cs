﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cinema.DAL.Domain
{
    public class Performance
    {
        public Performance()
        {
        }
        public Guid PerformanceId { get; set; }
        public string MovieName { get; set; }
        public string PerformanceHallNumber { get; set; }
        public DateTime MovieTime { get; set; }
        public decimal TicketPrice { get; set; }
        public Performance(Guid performanceId, string movieName, string performanceHallNumber, DateTime movieTime)
        {
            PerformanceId = performanceId;
            MovieName = movieName;
            PerformanceHallNumber = performanceHallNumber;
            MovieTime = movieTime;
        }
    }
}
