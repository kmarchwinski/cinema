﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cinema.DAL.Domain
{
    public class Order
    {
        public Order()
        {
        }
        public Guid OrderId { get; set; }
        public Guid MyPerformanceId { get; set; }
        public int SeatNumber { get; private set; }
        public int Row { get; private set; }
        public Order(Guid orderId, Guid myPerformanceId, int seatNumber, int row)
        {
            OrderId = orderId;
            MyPerformanceId = myPerformanceId;
            SetSeatNumber(seatNumber);
            SetRow(row);
        }
        public void SetSeatNumber(int seatNumber)
        {
            SeatNumber = seatNumber;
        }
        public void SetRow(int row)
        {
            Row = row;
        }
    }
}