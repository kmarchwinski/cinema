﻿using Cinema.DAL.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cinema.DAL.Context
{
    public class CinemaDbContext : DbContext
    {
        public CinemaDbContext(DbContextOptions<CinemaDbContext> options)
        : base(options)
        {
        }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<User> Users{ get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Performance> Performances { get; set; }
        public DbSet<Hall> Halls { get; set; }
    }
}
