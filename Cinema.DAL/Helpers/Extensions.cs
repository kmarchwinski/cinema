﻿using Cinema.DAL.Domain;
using Cinema.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cinema.DAL.Helpers
{
    public static class Extensions
    {
        public static PerformanceDTO MapPerformance (this Performance performance)
        {
            return new PerformanceDTO
            {
                MovieName = performance.MovieName,
                PerformanceHallNumber = performance.PerformanceHallNumber,
                MovieTime=performance.MovieTime,
                TicketPrice=performance.TicketPrice,
            };
        }
        public static HallDTO MapHall(this Hall hall)
        {
            return new HallDTO
            {
                HallNumber = hall.HallNumber,
                NumberOfSeats = hall.NumberOfSeats,
                Rows = hall.Rows,
                SeatsInRow = hall.SeatsInRow,
            };
        }
        public static OrderDTO MapOrder(this Order order)
        {
            return new OrderDTO
            {
                OrderId = order.OrderId,
                MyPerformanceId = order.MyPerformanceId,
                SeatNumber = order.SeatNumber,
                Row = order.Row,
            };
        }
        public static MovieDTO MapMovie(this Movie movie)
        {
            return new MovieDTO
            {
                Name = movie.Name,
                Length = movie.Length.ToString(),
            };
        }
        public static UserDTO MapUser(this User user)
        {
            return new UserDTO
            {
                Id = user.Id,
                Email = user.Email,
                Username = user.Username,
                Role = user.Role,
            };
        }
    }
}
