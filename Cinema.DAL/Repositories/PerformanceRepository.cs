﻿using Cinema.DAL.Context;
using Cinema.DAL.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cinema.DAL.Repositories
{
    public interface IPerformanceRepository
    {
        Task<Performance> GetAsync(Guid id);
        Task<IEnumerable<Performance>> GetAllAsync();
        Task AddAsync(Performance performance);
        Task UpdateAsync(Performance performance);
        Task RemoveAsync(Guid id);
    }
    public class PerformanceRepository : IPerformanceRepository
    {
        private readonly CinemaDbContext _dbContext;
        public PerformanceRepository(CinemaDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task AddAsync(Performance performance)
        {
            await _dbContext.Performances.AddAsync(performance);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<Performance>> GetAllAsync()
              => await _dbContext.Performances.ToListAsync();

        public async Task<Performance> GetAsync(Guid id)
            => await _dbContext.Performances.SingleOrDefaultAsync(x => x.PerformanceId == id);

        public async Task RemoveAsync(Guid id)
        {
            var performance = await GetAsync(id);
            _dbContext.Performances.Remove(performance);
            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(Performance performance)
        {
            _dbContext.Performances.Update(performance);
            await _dbContext.SaveChangesAsync();
        }
    }
}
