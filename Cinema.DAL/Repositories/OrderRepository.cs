﻿using Cinema.DAL.Context;
using Cinema.DAL.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema.DAL.Repositories
{
    public interface IOrderRepository
    {
        Task<Order> GetAsync(Guid id);
        Task<IEnumerable<Order>> GetAllAsync();
        Task AddAsync(Order order);
        Task UpdateAsync(Order order);
        Task RemoveAsync(Guid id);
        Task<bool> IsSeatReserved(Guid performanceId, int row, int seat);
    }
    public class OrderRepository : IOrderRepository
    {
        private readonly CinemaDbContext _dbContext;
        public OrderRepository(CinemaDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task AddAsync(Order order)
        {
            await _dbContext.Orders.AddAsync(order);
            await _dbContext.SaveChangesAsync();
        }
        public async Task<IEnumerable<Order>> GetAllAsync()
            => await _dbContext.Orders.ToListAsync();
        public async Task<Order> GetAsync(Guid id)
            => await _dbContext.Orders.SingleOrDefaultAsync(x => x.OrderId == id);
        public async Task RemoveAsync(Guid id)
        {
            var user = await GetAsync(id);
            _dbContext.Orders.Remove(user);
            await _dbContext.SaveChangesAsync();
        }
        public async Task UpdateAsync(Order order)
        {
            _dbContext.Orders.Update(order);
            await _dbContext.SaveChangesAsync();
        }
        public async Task<bool> IsSeatReserved(Guid performanceId, int row, int seat)
        {
            var IsSeatReserved = await _dbContext.Orders.AnyAsync(x => x.MyPerformanceId == performanceId && x.Row == row && x.SeatNumber == seat);
            return IsSeatReserved;
        }
    }
}
