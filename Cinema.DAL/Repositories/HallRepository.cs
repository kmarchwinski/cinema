﻿using Cinema.DAL.Context;
using Cinema.DAL.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cinema.DAL.Repositories
{
    public interface IHallRepository
    {
        Task<Hall> GetAsync(string hallNumber);
        Task<IEnumerable<Hall>> GetAllAsync();
        Task AddAsync(Hall hall);
        Task UpdateAsync(Hall hall);
        Task RemoveAsync(string hallNumber);
        Task<bool> DoesExist(string hallNumber);
    }
    public class HallRepository : IHallRepository
    {
        private readonly CinemaDbContext _dbContext;
        public HallRepository(CinemaDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<Hall> GetAsync(string hallNumber)
            => await _dbContext.Halls.SingleOrDefaultAsync(x => x.HallNumber == hallNumber);
        public async Task<IEnumerable<Hall>> GetAllAsync()
            => await _dbContext.Halls.ToListAsync();
        public async Task AddAsync(Hall hall)
        {
            await _dbContext.Halls.AddAsync(hall);
            await _dbContext.SaveChangesAsync();
        }
        public async Task RemoveAsync(string hallNumber)
        {
            var hall = await GetAsync(hallNumber);
            _dbContext.Halls.Remove(hall);
            await _dbContext.SaveChangesAsync();
        }
        public async Task UpdateAsync(Hall hall)
        {
            _dbContext.Halls.Update(hall);
            await _dbContext.SaveChangesAsync();
        }
        public async Task<bool> DoesExist(string hallNumber)
        {
            return await _dbContext.Halls.AnyAsync(x => x.HallNumber == hallNumber);
        }
    }
}
