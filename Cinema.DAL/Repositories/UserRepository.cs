﻿using Cinema.DAL.Context;
using Cinema.DAL.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cinema.DAL.Repositories
{
    public interface IUserRepository
    {
        Task<User> GetAsync(Guid id);
        Task<User> GetByEmailAsync(string email);
        Task<IEnumerable<User>> GetAllAsync();
        Task AddAsync(User user);
        Task UpdateAsync(User user);
        Task RemoveAsync(Guid id);
    }
    public class UserRepository : IUserRepository
    {
        private readonly CinemaDbContext _dbContext;
        public UserRepository(CinemaDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task AddAsync(User user)
        {
            await _dbContext.Users.AddAsync(user);
            await _dbContext.SaveChangesAsync();
        }
        public async Task<IEnumerable<User>> GetAllAsync()
            => await _dbContext.Users.ToListAsync();
        public async Task<User> GetAsync(Guid id)
            => await _dbContext.Users.SingleOrDefaultAsync(x => x.Id == id);
        public async Task<User> GetByEmailAsync(string email)
            => await _dbContext.Users.SingleOrDefaultAsync(x => x.Email == email);
        public async Task RemoveAsync(Guid id)
        {
            var user = await GetAsync(id);
            _dbContext.Users.Remove(user);
            await _dbContext.SaveChangesAsync();
        }
        public async Task UpdateAsync(User user)
        {
            _dbContext.Users.Update(user);
            await _dbContext.SaveChangesAsync();
        }
    }
}