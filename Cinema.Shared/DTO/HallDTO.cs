﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cinema.Shared.DTO
{
    public class HallDTO
    {
        public string HallNumber { get; set; }
        public int SeatsInRow { get;  set; }
        public int Rows { get;  set; }
        public int NumberOfSeats { get;  set; }
    }
}
