﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cinema.Shared.DTO
{
    public class PerformanceDTO
    {
        public string MovieName { get; set; }
        public string PerformanceHallNumber { get; set; }
        public DateTime MovieTime { get; set; }
        public decimal TicketPrice { get; set; }
    }
}
