﻿using Cinema.BLL;
using Cinema.BLL.services;
using Cinema.Shared;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cinema.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PerformanceController : ControllerBase
    {
        private readonly ILogger<PerformanceController> _logger;
        private readonly IPerformanceService _performanceService;
        private readonly IHallService _hallService;
        public PerformanceController(IPerformanceService performanceService, ILogger<PerformanceController> logger, IHallService hallService)
        {
            _performanceService = performanceService;
            _logger = logger;
            _hallService = hallService;
        }
        [HttpGet]
        [Route("details/{id}")]
        public async Task<JsonResult> Details(Guid id)
        {
            var performance = await _performanceService.GetById(id);
            return new JsonResult(performance);
        }
        [HttpGet]
        [Route("details/listofseats")]
        public async Task<JsonResult> ListOfSeats(Guid id)
        {
            var listOfSeats = await _performanceService.CreateHallVisualization(id);
            return new JsonResult(listOfSeats);
        }
        [HttpPost]
        [Route("add")]
        public async Task Add(Guid movieID, string performanceHallNumber, DateTime movieTime)
        {
            await _performanceService.AddPerformanceAsync(movieID, performanceHallNumber, movieTime);
        }
        [HttpGet]
        [Route("details/list")]
        public async Task<JsonResult> GetMoviesList()
        {
            var performances = await _performanceService.GetAllAsync();
            return new JsonResult(performances);
        }
        [HttpPost]
        [Route("remove/{id}")]
        public async Task Remove(Guid id)
        {
            await _performanceService.Remove(id);
        }
    }
}