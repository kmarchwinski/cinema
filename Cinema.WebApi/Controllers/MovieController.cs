﻿using Cinema.BLL;
using Cinema.Shared;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cinema.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MovieController : ControllerBase
    {
        private readonly ILogger<MovieController> _logger;
        private readonly IMovieService _movieService;
        public MovieController(IMovieService movieService, ILogger<MovieController> logger)
        {
            _movieService = movieService;
            _logger = logger;
        }
        [HttpGet]
        [Route("details/{id}")]
        public async Task<JsonResult> Details(Guid id)
        {
            var movie = await _movieService.GetById(id);
            return new JsonResult(movie);
        }
        [HttpGet]
        [Route("details/list")]
        public async Task<JsonResult> GetMoviesList()
        {
            var movies = await _movieService.GetAllAsync();
            return new JsonResult(movies);
        }
        [HttpPost]
        [Route("add")]
        public async Task Add(string name, int minutes)
        {
            await _movieService.AddMovieAsync(name.Trim().ToLowerInvariant(), minutes);

        }
        [HttpPost]
        [Route("remove/{id}")]
        public async Task Remove(Guid id)
        {
            await _movieService.Remove(id);
        }
    }
}