﻿using Cinema.BLL;
using Cinema.BLL.services;
using Cinema.DAL.Domain;
using Cinema.WebApi.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Cinema.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly IUserService _userService;
        public UserController(IUserService userService, ILogger<UserController> logger)
        {
            _userService = userService;
            _logger = logger;
        }
        [HttpGet]
        [Route("details/{id}")]
        public async Task<JsonResult> UserDetails(Guid id)
        {
            var user = await _userService.GetById(id);
            return new JsonResult(user);
        }
        [HttpGet]
        [Route("details/")]
        public async Task<JsonResult> UserDetails(string email)
        {
            var user = await _userService.GetByEmailAsync(email);
            return new JsonResult(user);
        }
        [HttpPost]
        [Route("login")]
        public async Task Login(AuthenticateRequest model)
        {
            await _userService.Authenticate(model);
        }
        [HttpPost]
        [Route("add")]
        public async Task Add(string email, string password, string username, string role)
        {
            await _userService.AddUserAsync(Guid.NewGuid(), email, password, username, role);
        }
        [ServiceFilter(typeof(AuthorizeToken))]
        [HttpGet]
        [Route("details/list")]
        public async Task<JsonResult> GetUsersList()
        {
            var users = await _userService.GetAllAsync();
            return new JsonResult(users);
        }
        [HttpPost]
        [Route("remove/{id}")]
        public async Task Remove(Guid id)
        {
            await _userService.RemoveAsync(id);
        }
    }
}