﻿using Cinema.BLL;
using Cinema.BLL.services;
using Cinema.DAL.Domain;
using Cinema.DAL.Repositories;
using Cinema.Shared;
using Cinema.Shared.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cinema.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrderController : ControllerBase
    {
        private readonly ILogger<OrderService> _logger;
        private readonly IOrderService _orderService;
        public OrderController(ILogger<OrderService> logger, IOrderService orderService)
        {
            _logger = logger;
            _orderService = orderService;
        }
        [HttpGet]
        [Route("details/{id}")]
        public async Task<JsonResult> Details(Guid id)
        {
            var order = await _orderService.GetbyId(id);
            return new JsonResult(order);
        }
        [HttpGet]
        [Route("list")]
        public async Task<JsonResult> OrdersList()
        {
            var orders = await _orderService.GetAllAsync();
            return new JsonResult(orders);
        }
        [HttpPost]
        [Route("add")]
        public async Task Add(Guid performanceId, List<BookedSeatDTO> bookedSeats)
        {
            await _orderService.SubmitOrder(performanceId, bookedSeats);
        }
        [HttpPost]
        [Route("remove/{id}")]
        public async Task Remove(Guid id)
        {
            await _orderService.RemoveOrder(id);
        }
    }
}
