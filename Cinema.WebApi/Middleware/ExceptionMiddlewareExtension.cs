using Microsoft.AspNetCore.Builder;
using System.Text.Json;

namespace Cinema.WebApi.Middleware
{
    public static class ExceptionMiddlewareExtension
    {

        public static void ConfigureCustomExceptionMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionHandlerMiddleware>();
        }
    }
}