using Cinema.DAL.Domain.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Cinema.WebApi.Middleware
{
    public class ExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<Exception> _logger;
        public ExceptionHandlerMiddleware(RequestDelegate next, ILogger<Exception> logger)
        {
            _next = next;
            _logger = logger;
        }
        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception exception)
            {
                await HandleExceptionAsync(context, exception);
            }
        }
        private Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var errorCode = "error";
            var statusCode = HttpStatusCode.BadRequest;
            var errorMessage = string.Empty;
            var exceptionType = exception.GetType();
            var guid = Guid.NewGuid();
            switch (exception)
            {
                case UnauthorizedAccessException e when exceptionType == typeof(UnauthorizedAccessException):
                    statusCode = HttpStatusCode.Unauthorized;
                    break;
                case CinemaException e when exceptionType == typeof(CinemaException):
                    statusCode = HttpStatusCode.BadRequest;
                    errorCode = e.Code;
                    errorMessage = e.Message;
                    break;
                default:
                    statusCode = HttpStatusCode.InternalServerError;
                    _logger.LogError($"{guid} | {exception.ToString()}");
                    break;
            }
            var response = new { 
                code = errorCode, 
                message = errorMessage, 
                correlationId = guid };

            var payload = JsonConvert.SerializeObject(response);
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)statusCode;
            return context.Response.WriteAsync(payload);
        }
    }
}